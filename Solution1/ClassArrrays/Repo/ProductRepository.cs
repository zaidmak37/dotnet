﻿using System;
using ClassArrays.Models;

namespace ClassArrays.Repository
{
    internal class ProductRepository
    {
        Product[] productList = new Product[4];

        public ProductRepository()
        {
            productList[0] = new Product(1, "Samsung", "Mobile", 1000);
            productList[1] = new Product(2, "Redmi", "Mobile", 1500);
            productList[2] = new Product(3, "Toshiba", "TV", 15000);
            productList[3] = new Product(4, "Dell", "Laptop", 50000);
        }

        public Product[] GetAllProdducts()
        {
            return this.productList;
        }

        public Product[] GetProductsByCategory(string cat)
        {
            return Array.FindAll(this.productList, item => item.Category == cat);

        }
        public Product[] GetProductsByName(string name)
        {
            return Array.FindAll(this.productList, item => item.Name == name);

        }
        public void DisplayAllProducts()
        {
            for (int i = 0; i < this.productList.Length; i++)
            {
                Product item = this.productList[i];
                Console.WriteLine($"{item}");
            }
        }
        public static void DisplayProducts(Product[] products)
        {
            for (int i = 0; i < products.Length; i++)
            {
                Product item = products[i];
                Console.WriteLine($"{item}");
            }
        }
        public void DeleteProduct(int id)
        {
            this.productList = Array.FindAll(this.productList, item => item.Id != id);
        }

        public void UpdateProduct(int id, string name, string cat, double price)
        {
            int indx = Array.FindIndex(productList, item => item.Id == id);
            if (indx < 0)
            {
                Console.WriteLine("Such product doesn't exist");
                return;
            }
            if (name != null && name != "")
            {
                productList[indx].Name = name;
            }
            if (cat != null && cat != "")
            {
                productList[indx].Category = cat;
            }
            if (price > 0)
            {
                productList[indx].Price = price;
            }
        }
    }
}

