﻿
using ClassArrays.Models;
using ClassArrays.Repository;
ProductRepository prepo = new ProductRepository();
prepo.DisplayAllProducts();
Console.WriteLine("Plz enter id. for the item to be removed");
int pid = int.Parse(Console.ReadLine().Trim());
prepo.DeleteProduct(pid);
Console.WriteLine("\n");
prepo.DisplayAllProducts();
Console.WriteLine("Plz enter id. for the item to be updated");
int pupid = int.Parse(Console.ReadLine().Trim());
Console.WriteLine("Enter new Name.");
string pname = Console.ReadLine().Trim();
Console.WriteLine("Enter new Category.");
string pcat = Console.ReadLine().Trim();
Console.WriteLine("Enter new Price");
double pprice = double.Parse(Console.ReadLine().Trim());
prepo.UpdateProduct(pupid, pname, pcat, pprice);
prepo.DisplayAllProducts();
Console.WriteLine("\n");

