﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day3.models
{

    internal class Models
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public int Price { get; set; }
        public float Rating { get; set; }

        public Models(string name, string category, int price, float rating)
        {
            Name = name;
            Category = category;    
            Price = price;
            Rating = rating;
        }



    }
}
