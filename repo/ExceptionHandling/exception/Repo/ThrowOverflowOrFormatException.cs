﻿using System.Runtime.Serialization;

namespace exception.Repo
{
    [Serializable]
    internal class ThrowOverflowOrFormatException : Exception
    {
        public ThrowOverflowOrFormatException()
        {
        }

        public ThrowOverflowOrFormatException(string? message) : base(message)
        {
        }

        public ThrowOverflowOrFormatException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected ThrowOverflowOrFormatException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}