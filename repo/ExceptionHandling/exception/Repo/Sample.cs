﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exception.Repo
{
    internal class Sample
    {
        public void Convert()
        {
            Console.WriteLine("Enter a int value");
            string a = Console.ReadLine();
            try
            {
                int b = int.Parse(a);
                Console.WriteLine(b);
            }
            catch (System.FormatException e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
