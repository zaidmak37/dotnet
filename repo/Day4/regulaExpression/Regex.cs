﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace regulaExpression
{
    internal static class Regularex
    {
        
            internal static string emailPatern = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";


            internal static bool ValidateEmail(string enmail)
            {
                if (enmail != null)
                {
                    return Regex.IsMatch(enmail, emailPatern);
                }
                return false;
            
        }
    }
}
