﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritence.Models
{
    internal class Parentcla
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public string Category { get; set; }
        public int quantity;
        public Parentcla(string name, int price, string category, int quantity)
           {
            this.Name = name;
            this.Price = price;
            this.Category = category;
            this.quantity = quantity;
            }
    }
}
