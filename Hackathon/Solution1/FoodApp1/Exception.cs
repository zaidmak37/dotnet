﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodApp1
{
    internal class NoItemsInCart : Exception
    {
        public NoItemsInCart(string mess) : base(mess)
       {

        }
    }
}
