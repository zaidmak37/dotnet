﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodApp1.Models
{
    internal class Food
    {
        public int Price { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public  int Quantity { get; set; }
        public override string ToString()
        {
            return $"Id::{Id}\t Name::{Name}\t Price::{Price}";
        }
    }
}
