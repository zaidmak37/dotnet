﻿using FoodApp1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FoodApp1.Models.CustomerInfo;

namespace FoodApp1.Repo
{
#region listcreation 
    internal class UserRepo : Iadd
    {
        string user;
        String[] LocationVerify = new string[] {"Mumbai","Chennai","Delhi","Banglore"};
        List<CustomerInfo> CustomerInfo;
        List<Food> foodDetails =new List<Food>()
        {
            new Food(){Name = "Food1", Price = 200, Id = 1},
            new Food(){Name = "Food2", Price = 300, Id = 2},
            new Food(){Name = "Food3", Price = 400, Id = 3},
            new Food(){Name = "Food4", Price = 500, Id = 4}
        };

        #endregion
#region UserInput
        public void Userinfo()
        {
            info:
            Console.WriteLine("Enter the city name");
            string Locat = Console.ReadLine();
            bool isvalid = LocationVerify.First() == Locat;

            if (isvalid)
            {
               
                    Console.WriteLine("Enter The Username");
                    user = Console.ReadLine();
              
                CustomerInfo = new List<CustomerInfo>()
                {
                    new CustomerInfo(){Name = user, Location = Locat}
                };
                MainMenu();
            }
            else
            {
                Console.WriteLine("Sorry We donot deliver at your location");
                goto info;
            }
        }
        #endregion
#region Main menu
        public void MainMenu()
        {
            start:
            Console.WriteLine("Press 1 to View Menu");
            Console.WriteLine("Press 2 to Generate Bill");
            Console.WriteLine("Press 3 to View Cart");
            Console.WriteLine("Press 4 to Exit");
            try
            {


                int val = int.Parse(Console.ReadLine());
                switch (val)
                {
                    case 1:
                        ViewMenu();
                        break;
                    case 2:
                        GenerateBill();
                        break;
                    case 3:
                        ViewCart();
                        break;
                    case 4:
                        ExitMain();
                        break;
                    default:
                        Console.WriteLine("Invalid option");
                        break;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("\n \n -----Please Enter a Integer type value----- \n \n ");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                goto start;
            }
               
            }
        #endregion
#region Exit
        private void ExitMain()
        {
            Console.WriteLine("");
        }
        #endregion
#region viewCart
        private void ViewCart()
        {

            Console.WriteLine($"Hello {user} you have ordered");
            Console.WriteLine("--------------------------------------------------");
            foreach (Food food in foodDetails)
            {
                Console.WriteLine($"{food}\t Quantity :: {food.Quantity}");
            }

        }
        #endregion
#region GenerateBill

        private void GenerateBill()
        {
            int grandTotal = 0;
            Console.WriteLine("--------------------------------------------------------");
            Console.WriteLine($"Hello {user} you have added");
            
            foreach (Food item in foodDetails)
            {
                
                if (item.Quantity != 0)
                {
                    int total;
                    total = item.Quantity * item.Price;
                    
                    Console.WriteLine( "\n");
                    Console.WriteLine($" Name :: {item.Name}\t Quantity :: {item.Quantity}\t Price :: {item.Price}\t Total :: {total}");
                    Console.WriteLine("\n");

                    grandTotal += total;
                }
                else
                {
                    throw new NoItemsInCart("No items are present");
                }
            }
            Console.WriteLine("Your grand Total is:");
            Console.WriteLine($" \n {grandTotal *.15} \n");
            Console.WriteLine( "----------------------------------------------------");
            MainMenu();

        }
        #endregion
#region ViewMenu

        private void ViewMenu()
        {
           foreach(Food f1 in foodDetails)
            {
                Console.WriteLine($"{f1}");
            }

            AddItems();
            

            MainMenu();
          
        }
        #endregion
#region AddItems

        public void AddItems()
        {
            Console.WriteLine("To buy now Press1 \t Add to Cart Press 2");
            int addBuy = int.Parse(Console.ReadLine());
            if(addBuy == 1)
            {
                AddOrBuy();
                GenerateBill();

            }
            else if(addBuy == 2)
            {
                AddOrBuy();
            }



        }
        private void AddOrBuy()
        {
        addItem:
            Console.WriteLine("Enter the Id Of the food you want to Buy");
            int order = int.Parse(Console.ReadLine());
            switch (order)
            {
                case 1:
                    foodDetails[0].Quantity += 1;
                    break;
                case 2:
                    foodDetails[1].Quantity += 1;
                    break;
                case 3:
                    foodDetails[2].Quantity += 1;
                    break;
                case 4:
                    foodDetails[3].Quantity += 1;
                    break;

            }
            Console.WriteLine("type yes if you want to continue adding food to your cart");
            string addi = Console.ReadLine();

            if (addi == "yes")
            {
                goto addItem;
            }
        }
        #endregion
    }
}