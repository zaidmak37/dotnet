﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionLists.Models
{
    internal class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int Price { get; set; }

        public override string ToString()
        {
            return $"Name :: {Name}\t Id :: {Id}\t Price :: {Price}";
        }
    }
}
