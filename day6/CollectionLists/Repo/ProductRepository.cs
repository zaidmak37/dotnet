﻿using CollectionLists.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionLists.Repo
{
    internal class ProductRepository
    {
        string Filename = "Data.txt";
        List<Product> products = new List<Product>()
            {
                new Product() { Id = 1, Name = "Samsung", Price = 10000},
                new Product() { Id = 2, Name = "Hitachi", Price = 23000},
                new Product() { Id = 3, Name = "LG", Price = 53000},
                new Product() { Id = 4, Name = "Samsung", Price = 50000},
            };

        public ProductRepository()
        {

        }
        public void GetProducts()
        {

            using (StreamReader sr = new StreamReader(Filename))
            {
                string[] prod = sr.ReadLine().Split(" ");
                for (int i = 0; i < prod.Length; i++)
                {
                    Console.WriteLine(prod[i]);
                }

            }
        }
        public bool AddProduct(Product product)
        {
            foreach (Product p in products)
            {
                if (p.Id == product.Id)
                {
                    Console.WriteLine("The item already exists");
                    return false;
                }
            }
            products.Add(product);
            using (StreamWriter sw = new StreamWriter(Filename))

            {
                string combinedString = string.Join(",", products);
                sw.WriteLine(combinedString);

            }
            return true;


        }
    }
}

