﻿using CollectionLists.Models;
using CollectionLists.Repo;

Product product = new Product() { Id= 6,Name= "Mitashi", Price = 33000};
ProductRepository productRepository = new ProductRepository();
productRepository.AddProduct(product);
productRepository.AddProduct(new Product() { Id = 7, Name = "Mitashi", Price = 33000 });
productRepository.GetProducts();
