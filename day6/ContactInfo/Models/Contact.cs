﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactInfo.Models
{
    internal class Contact
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int PhoneNumber { get; set; }

        public override string ToString()
        {
            return $"{Name}, {City},{Address},{PhoneNumber}";
        }
    }
}
