﻿using ContactInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactInfo.Repo
{
    internal class UserRepo
    {
        List<Contact> contacts = new List<Contact>()
        {
            new Contact(){ Name= "A", Address = "DSAd" , City= "dsasd", PhoneNumber = 3213},
            new Contact(){ Name= "B", Address = "DSAd" , City= "dsasd", PhoneNumber = 32133},
            new Contact(){ Name= "C", Address = "DSAd" , City= "dsasd", PhoneNumber = 32134},
            new Contact(){ Name= "D", Address = "DSAd" , City= "dsasd", PhoneNumber = 32135}
        };
        
        public void AddContact(Contact contact1)
        {
            contacts.Add(contact1);
            
            
        }

       public bool DeleteContact()
        {
            Console.WriteLine("Enter the name of contacts you want to delete:");
            string rem = Console.ReadLine();
           foreach(Contact item in contacts)
            {
                if(item.Name== rem)
                {
                    contacts.Remove(item);
                    return true;
                }
            }
            return false;

        }
        public void UpdateContact()
        {
            Console.WriteLine( "Enter the name of the contact ypou want to update");
            string remov = Console.ReadLine();
            foreach( Contact item in contacts)
            {
                if(item.Name  == remov)
                {
                    Console.WriteLine("Enter the New Name:");
                    item.Name = Console.ReadLine();
                    Console.WriteLine("Enter the New city name:");
                    item.City = Console.ReadLine();
                    Console.WriteLine("Enter the New address:");
                    item.Address = Console.ReadLine();
                    Console.WriteLine("Enter the New phone:");
                    item.PhoneNumber = int.Parse(Console.ReadLine());

                }
            }

        }
        public void Display()
        {
            foreach(Contact item1 in contacts)
            {
                Console.WriteLine(item1.ToString());
                Console.WriteLine("\n");
            }    
        }
    }
}