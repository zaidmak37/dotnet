﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqQuery.Models
{
    internal class ShoppingQuantity
    {
        public int ShoppingId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
