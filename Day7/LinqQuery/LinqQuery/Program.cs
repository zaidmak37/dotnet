﻿using LinqQuery.Models;

ProductInfo[] productInfo = new ProductInfo[]
{
    new ProductInfo(){Name = " Iphone 11", Id=1, Price = 30000},
    new ProductInfo(){Name = " Iphone 12", Id=2, Price = 40000},
    new ProductInfo(){Name = " Iphone 13", Id=3, Price = 50000},
    new ProductInfo(){Name = " Iphone 10", Id=4, Price = 20000}
};
ShoppingQuantity[] productQuantity = new ShoppingQuantity[]
{
    new ShoppingQuantity() {ShoppingId = 1, ProductId = 1, Quantity = 10},
    new ShoppingQuantity() {ShoppingId = 2, ProductId = 2, Quantity = 23},
    new ShoppingQuantity() {ShoppingId = 3, ProductId = 3, Quantity = 14},
    new ShoppingQuantity() {ShoppingId = 4, ProductId = 4, Quantity = 22}

};

var Result = from p in productInfo
             join p1 in productQuantity 
             on p.Id equals p1.ProductId 

             select new
             {
                 proid = p.Id,
                 proName  = p.Name,
                 proQuantity = p1.Quantity


             };
foreach(var result in Result)
{
    Console.WriteLine($"NAme :: {result.proName}\t ID :: {result.proid}\t Quantity :: {result.proQuantity}");
}
